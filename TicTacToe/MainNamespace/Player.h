#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED
#include <iostream>
#include "Enums.cpp"
using namespace std;

class Player
{
	private:
		string mName;
		char mSign;
		static Turn mPlayerTurn;
	public :
		Player();
		//Player(char);
		//Player(string, char);
		~Player();
		string GetName();
		char GetSign();
		static Turn GetTurn();
		void ChangeTurn();
		void Play(char*, int);
};

#endif