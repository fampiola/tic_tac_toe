#include <iostream>
#include "Player.h"
#include "ArrayManipulation.h"
using namespace std;

//All main() invokable functions
void InitialInfo(Player*, Player*);

int main()
{
    //All Variables declaration
    char TicTacToeArray[9];
    int arrayLength = sizeof(TicTacToeArray) / sizeof(TicTacToeArray[0]);
    fill_n(&TicTacToeArray[0], arrayLength, '_');
    Player player1;
    Player player2;
    Player* playerTurn = &player1;
    ArrayManipulation arrayHandler(player1.GetSign(), player2.GetSign());

    //Game starts
    InitialInfo(&player1, &player2);
    while (arrayHandler.AnyEmptyCell(TicTacToeArray, arrayLength) && !arrayHandler.IsWinner(TicTacToeArray))
    {
        arrayHandler.PrintArray(TicTacToeArray, arrayLength);
        playerTurn = (Player::GetTurn() == Player1) ? &player1 : &player2;
        cout << playerTurn->GetName() << " is playing !" << endl << endl;
        playerTurn->Play(TicTacToeArray, arrayLength); 
    }
    playerTurn->ChangeTurn();
    string terminationMessage = arrayHandler.IsWinner(TicTacToeArray) ? "Game terminated. " + playerTurn->GetName() + " WINS!" : "Game terminated. DRAW!";
    cout << terminationMessage;
}

//Prints the initial information of players name and their signs
void InitialInfo(Player* _player1, Player* _player2)
{
    system("cls");
    cout << "Player 1 is " << _player1->GetName() << " with sign : " << _player1->GetSign() << endl;
    cout << "Player 2 is " << _player2->GetName() << " with sign : " << _player2->GetSign() << endl;
}