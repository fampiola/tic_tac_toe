#ifndef ARRAYMANIPULATION_H_INCLUDED
#define ARRAYMANIPULATION_H_INCLUDED
#include <iostream>
using namespace std;

class ArrayManipulation
{
	private:
		char mSignPlayer1;
		char mSignPlayer2;
	public:
		ArrayManipulation();
		ArrayManipulation(char, char);
		~ArrayManipulation();
		void PrintArray(char*, int);
		bool AnyEmptyCell(char*, int);
		bool IsWinner(char*);
};

#endif