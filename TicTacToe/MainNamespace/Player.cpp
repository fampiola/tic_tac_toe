#include "Player.h"

Turn Player::mPlayerTurn = Player1;

Player::Player()
{
    mSign = (mPlayerTurn == Player1) ? 'X' : 'Y';
    ChangeTurn();
    cout << "Enter player name" << endl;
    cin >> mName;
}

//Player::Player(char _sign)
//{
//    mSign = _sign;
//    cout << "Enter player name" << endl;
//    cin >> mName;
//}

//Player::Player(string _name, char _sign)
//{
//	mSign = _sign;
//    mName = _name;
//}

Player::~Player()
{

}

string Player::GetName()
{
	return mName;
}

char Player::GetSign()
{
	return mSign;
}

Turn Player::GetTurn()
{
    return mPlayerTurn;
}

void Player::ChangeTurn()
{
    mPlayerTurn = (mPlayerTurn == Player1) ? Player2 : Player1;
}

//This function takes as argument a pointer that indicates
//to the 1st element of a char[] and the length of this array.
//Asks the coordinates from the player, checks the range validity,
//and allows the user to fill in the position (x,y) his sign.
void Player::Play(char* _arrayPointer, int _arrayLength)
{
    int _positionX = -1;
    int _positionY = -1;
    bool CorrectCoordinates = false;
    cout << "Enter the coordinates (x,y) [MIN:(1,1) & MAX:(3,3)]" << endl;

    do
    {
        cin >> _positionX;
        CorrectCoordinates = (_positionX < 1) || (_positionX > 3) ? false : true;
        if (!CorrectCoordinates)
            cout << "The coordinates X you entered is out of range, please enter again [MIN:(1,1) & MAX:(3,3)]" << endl;
    } while (!CorrectCoordinates);

    do
    {
        cin >> _positionY;
        CorrectCoordinates = (_positionY < 1) || (_positionY > 3) ? false : true;
        if (!CorrectCoordinates)
            cout << "The coordinates Y you entered is out of range, please enter again [MIN:(1,1) & MAX:(3,3)]" << endl;
    } while (!CorrectCoordinates);

    int _position = (int(sqrt(_arrayLength)) * _positionX) + _positionY - 4;
    bool result = (_arrayPointer[_position] == '_') ? true : false;
    if (result)
    {
        _arrayPointer[_position] = mSign;
        ChangeTurn();
        system("cls");
    }
    else
    {
        system("cls");
        cout << "The cell is not empty, try another." << endl;
    }
}