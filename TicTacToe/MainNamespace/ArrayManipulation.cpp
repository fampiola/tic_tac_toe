#include "ArrayManipulation.h"

ArrayManipulation::ArrayManipulation()
{
    mSignPlayer1 = 'X';
    mSignPlayer2 = 'O';
}

ArrayManipulation::ArrayManipulation(char _signPlayer1, char _signPlayer2)
{
    mSignPlayer1 = _signPlayer1;
    mSignPlayer2 = _signPlayer2;
}

ArrayManipulation::~ArrayManipulation()
{

}

//This function prints all elements of a 1D array
//in the form of a 2D array
void ArrayManipulation::PrintArray(char *_array, int _arrayLength)
{
    for (int i = 0; i < _arrayLength; i++)
    {
        cout.width(10);
        cout << _array[i];
        if (((i + 1) % int(sqrt(_arrayLength))) == 0)
            cout << endl << endl;
    }
    cout << endl << endl;
}

//Check if there is any empty cell to be filled on the next move
bool ArrayManipulation::AnyEmptyCell(char* _array, int _arrayLength)
{
    bool isEmpty = true;
    for (int i = 0; i < _arrayLength; i++)
    {
        isEmpty = (_array[i] == '_') ? true : false;
        if (isEmpty)
            break;
    }
    //If there is any empty cell, return true
    return isEmpty;
}

//Check if there exists tic tac toe, then there is a winner
bool ArrayManipulation::IsWinner(char* _array)
{
    bool isWinner = false;
    if ((_array[0] == mSignPlayer1 && _array[1] == mSignPlayer1 && _array[2] == mSignPlayer1) || (_array[0] == mSignPlayer2 && _array[1] == mSignPlayer2 && _array[2] == mSignPlayer2))
        isWinner = true;
    else if ((_array[3] == mSignPlayer1 && _array[4] == mSignPlayer1 && _array[5] == mSignPlayer1) || (_array[3] == mSignPlayer2 && _array[4] == mSignPlayer2 && _array[5] == mSignPlayer2))
        isWinner = true;
    else if ((_array[6] == mSignPlayer1 && _array[7] == mSignPlayer1 && _array[8] == mSignPlayer1) || (_array[6] == mSignPlayer2 && _array[7] == mSignPlayer2 && _array[8] == mSignPlayer2))
        isWinner = true;
    else if ((_array[0] == mSignPlayer1 && _array[3] == mSignPlayer1 && _array[6] == mSignPlayer1) || (_array[0] == mSignPlayer2 && _array[3] == mSignPlayer2 && _array[6] == mSignPlayer2))
        isWinner = true;
    else if ((_array[1] == mSignPlayer1 && _array[4] == mSignPlayer1 && _array[7] == mSignPlayer1) || (_array[1] == mSignPlayer2 && _array[4] == mSignPlayer2 && _array[7] == mSignPlayer2))
        isWinner = true;
    else if ((_array[2] == mSignPlayer1 && _array[5] == mSignPlayer1 && _array[8] == mSignPlayer1) || (_array[2] == mSignPlayer2 && _array[5] == mSignPlayer2 && _array[8] == mSignPlayer2))
        isWinner = true;
    else if ((_array[0] == mSignPlayer1 && _array[4] == mSignPlayer1 && _array[8] == mSignPlayer1) || (_array[0] == mSignPlayer2 && _array[4] == mSignPlayer2 && _array[8] == mSignPlayer2))
        isWinner = true;
    else if ((_array[2] == mSignPlayer1 && _array[4] == mSignPlayer1 && _array[6] == mSignPlayer1) || (_array[2] == mSignPlayer2 && _array[4] == mSignPlayer2 && _array[6] == mSignPlayer2))
        isWinner = true;
    else
        isWinner = false;
    //If there is a winner, return true
    return isWinner;
}
